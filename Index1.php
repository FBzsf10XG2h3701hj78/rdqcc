
<!--

//session_start(); 
/*$_SESSION["RdPDM_MofT"] = $AUP_Row["RdU_MofT"];

if (!isset($_SESSION["User_ID"]) || $_SESSION["User_ID"]=="")
	{
		$ERR_MSG = "Time out, Please Re-Sign in";
		echo "<script>window.location='index.php?ERR_MSG=$ERR_MSG'</script>";
	} 

$User_ID = strtoupper($_SESSION["User_ID"]); 
include "MyraTrain_DB.php";

?>-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="icon" type="Images/x-icon" href="Images/RdPDM.ico"/>

<title>RDQCC</title>

<link rel="stylesheet" type="text/css" href="CSS/ShowRoom.css">
<!--<link rel="stylesheet" type="text/css" href="CSS/RdPDM.css">-->
<style typestyletype="text/css"> 

/*---Global---*/
</style> 

<link rel="stylesheet" type="text/css" href="SCRIPTS/easyui.css">
<link rel="stylesheet" type="text/css" href="SCRIPTS/icon.css">

<--script type="text/javascript" src="SCRIPTS/jquery-1.6.1.min.js"></script-->
<script type="text/javascript" src="SCRIPTS/jquery-3.1.1.js"></script>
<script type="text/javascript" src="SCRIPTS/jquery.easyui.min.js"></script>
    
<script type="text/javascript">



/*--------function 2: click on Login to pull down a toggle window-----------------*/
$(document).ready(function() {
   $("#login").click(function() {
	   $("#login").toggleClass("minus");
	   $("#loginDiv form").slideToggle();
   });
});

/*--------function 3: handle slider // adapted from https://codepen.io/zuraizm/pen/vGDHl ----------------*/
jQuery(document).ready(function ($) {

   var slideCount = $('#slider ul li').length;
   var slideWidth = $('#slider ul li').width();
   var slideHeight = $('#slider ul li').height();
   var sliderUlWidth = slideCount * slideWidth;
   var secondsBetweenSlides = 4;

   setInterval(moveRight, secondsBetweenSlides*1000);     
        
   $('#slider').css({ width: slideWidth, height: slideHeight });
	
   $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
   $('#slider ul li:last-child').prependTo('#slider ul');

   function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    $('a.control_prev').click(function () {
        moveLeft();
    });

    $('a.control_next').click(function () {
        moveRight();
    });

}); 

</script>	

</head>
<body style="height:100%;">
<div ID="Container">

<!---------------------Part I: Head Navigation section (background:url(images/t-back.gif) repeat-x;------------------------->
<!-------------------------------------------------------------------------------------------------------------------------->

<table ID="Web_Navigation">				<!--use a table to organize entire navigation row-->

										<!---1-----------------Myra Train Logo-------------------->	
<tr>	
<td ID="Web_RDQCC_Logo"><img src="Images/logo.jpg" alt="RDQCC Logo" height="70px" /></td>

										<!---2-----------------Navigation------------------------->	
<td ID="Web_Navigation_MainMenu">       							

<a href="Index.php" class="easyui-linkbutton" data-options="plain:true" menu="#mm1"><span class="Web_Menu_Button">Home</span></a> &nbsp; <!--slides-->
<a href="Pages/About_Us.php" class="easyui-linkbutton" data-options="plain:true" menu="#mm2"><span class="Web_Menu_Button">About Us</span> </a> &nbsp;
<a href="Pages/Product_RdPDM1.php" class="easyui-linkbutton" data-options="plain:true" menu="#mm3"><span class="Web_Menu_Button">Products</span> </a> &nbsp;
<a href="Pages/Events.php" class="easyui-linkbutton" data-options="plain:true" menu="#mm4"><span class="Web_Menu_Button">Events</span></a> &nbsp; 
<a href="Pages/Testmonies.php" class="easyui-linkbutton" data-options="plain:true" menu="#mm5"><span class="Web_Menu_Button">Testimonials</span></a> &nbsp; 
<a href="Pages/Contact.php" class="easyui-linkbutton" data-options="plain:true" menu="#mm6"><span class="Web_Menu_Button">Contact</span></a> &nbsp; 

</td>	
						
										<!---3----section for toggle login-------->
<td ID="Toggle_Login">
<div ID="loginDiv">         
       <!-- when this is clicked on the below form should be displayed and plus sign should change to minus sign-->
    <p id="login" class="plus">Login</p> 
       
 	<form name="Feedback_Form" action="" method="post" onkeydown="if(event.keyCode==13 || event.keyCode==116) return false;" onsubmit="Check_RdU()">
	<p>
	<label for="username">Username:</label>
	<input type="text" name="Username" id="username">
	</p>
	
	<p>
	<label for="pw">Password:</label>
	<input type="password" name="Password" id="pw">
	</p>
	
	<p>
    <input type="submit" name="Login_Submit" value="Submit">
	</p>
	   
	   <!-- placeholder for response if form data is correct/incorrect --> 
	<p id="error"> </p>
    </form>
	</div>
</td>										
</tr>
</table>
 
<!---------------------Part II: Working area-------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------------------->			
<table ID="Web_MainContent" align="center">
<tr>											
<td ID="Home_Image">
    <div id="slider">
        <a href="#" class="control_next">&raquo;</a>
        <a href="#" class="control_prev">&laquo;</a>
        <ul>
          <li>
              <h3>VISUALIZE PRODUCT DEVELOPMENT</h3>
              <h4>Templates for hardware, software, hardware+software, system, disposable...</h4>
              <img src="Images/IMG_11.jpg" alt="Product_Slides" width="1400px">
          </li>
          <li>
              <h3>INFORMATION UNDER ONE ROOF</h3>
              <h4>Develiverables, human power, schedules, relevants, approverlists...</h4>
              <img src="Images/IMG_21.jpg" alt="Product_Slides" width="1400px">              
          </li>
          <li>
              <h3>CUSTOMIZE PROJECT ROADMAP</h3>
              <h4>It is so simply, you just need to drag and drop deliverables...</h4>
              <img src="Images/IMG_31.jpg" alt="Product_Slides" width="1400px">
          </li>
          <li>
              <h3>MONITOR PROJECT OPERATION</h3>
              <h4>Real time dashboard, statistical charts, deliverable status, critical path...</h4>
              <img src="Images/IMG_41.jpg" alt="Product_Slides" width="1400px">
          </li>
        </ul>  
    </div>
</td> 
</tr>
</table> 

<!---------------------Part III: Footer area-------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------------------->			
<table ID="Bottom_Bar"> 
<tr>   
<td><span class="Footer_Words">Copyright &copy; 2015-2017 Risk Driven Quality Consulting Co. (RDQCC)</span></td>
</tr>              
</table> 
   

</div>
</body>
</html>
