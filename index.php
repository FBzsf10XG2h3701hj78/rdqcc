﻿<?php
session_start(); 

$User_ID = strtoupper($_SESSION["User_ID"]); 

?>
<!DOCTYPE html>
<!-- for Myra train 2-10-2017 -->
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    
 <script type="text/javascript" src="SCRIPTS/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="SCRIPTS/jssor.slider.mini.js"></script>
    <!-- use jssor.slider.debug.js instead for debug -->
 <script>
        jQuery(document).ready(function ($) {

            var jssor_1_SlideoTransitions = [
              [{ b: 5500, d: 3000, o: -1, r: 240, e: { r: 2 } }],
              [{ b: -1, d: 1, o: -1, c: { x: 51.0, t: -51.0 } }, { b: 0, d: 1000, o: 1, c: { x: -51.0, t: 51.0 }, e: { o: 7, c: { x: 7, t: 7 } } }],
              [{ b: -1, d: 1, o: -1, sX: 9, sY: 9 }, { b: 1000, d: 1000, o: 1, sX: -9, sY: -9, e: { sX: 2, sY: 2 } }],
              [{ b: -1, d: 1, o: -1, r: -180, sX: 9, sY: 9 }, { b: 2000, d: 1000, o: 1, r: 180, sX: -9, sY: -9, e: { r: 2, sX: 2, sY: 2 } }],
              [{ b: -1, d: 1, o: -1 }, { b: 3000, d: 2000, y: 180, o: 1, e: { y: 16 } }],
              [{ b: -1, d: 1, o: -1, r: -150 }, { b: 7500, d: 1600, o: 1, r: 150, e: { r: 3 } }],
              [{ b: 10000, d: 2000, x: -379, e: { x: 7 } }],
              [{ b: 10000, d: 2000, x: -379, e: { x: 7 } }],
              [{ b: -1, d: 1, o: -1, r: 288, sX: 9, sY: 9 }, { b: 9100, d: 900, x: -1400, y: -660, o: 1, r: -288, sX: -9, sY: -9, e: { r: 6 } }, { b: 10000, d: 1600, x: -200, o: -1, e: { x: 16 } }]
            ];

            var jssor_1_options = {
                $AutoPlay: true,
                $SlideDuration: 800,    /*800*/
                $SlideEasing: $Jease$.$OutQuint,
                $CaptionSliderOptions: {
                    $Class: $JssorCaptionSlideo$,
                    $Transitions: jssor_1_SlideoTransitions
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing

            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
		
		
<!--------function 2: click on Login to pull down a toggle window----------------->
$(document).ready(function() {
   $("#login").click(function() {
	   $("#login").toggleClass("minus");
	   $("#loginDiv form").slideToggle();
   });
});

/*--------function 3: click on submit button---------
$(document).ready(function() {
   $("#loginDiv input[type=button]").click(function() {
	  if ($("#username").val() == '') {
		  $("#error").text("Please enter Username!");
	  }
	  else if ($("#pw").val() =='') {
		  $("#error").text("Please enter password!");
	  }
	  else {
		  $("#error").text("Please stand by!");
	  }
   });
});
*/

		
		
		
</script>

<style>
        /* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
.jssorb05 {
 	position: absolute;}

.jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
    position: absolute;
                /* size of bullet elment */
    width: 16px;
    height: 16px;
    background: url('Images/IMG_dot.png') no-repeat;
    overflow: hidden;
    cursor: pointer;}

.jssorb05 div {
    background-position: -7px -7px;}

.jssorb05 div:hover, .jssorb05 .av:hover {
    background-position: -37px -7px;}

.jssorb05 .av {
    background-position: -67px -7px;}

.jssorb05 .dn, .jssorb05 .dn:hover {
    background-position: -97px -7px;}
            
        /* jssor slider arrow navigator skin 22 css */
        /*
        .jssora22l                  (normal)
        .jssora22r                  (normal)
        .jssora22l:hover            (normal mouseover)
        .jssora22r:hover            (normal mouseover)
        .jssora22l.jssora22ldn      (mousedown)
        .jssora22r.jssora22rdn      (mousedown)
        .jssora22l.jssora22lds      (disabled)
        .jssora22r.jssora22rds      (disabled)
        */
.jssora22l, .jssora22r {
    display: block;
    position: absolute;
            /* size of arrow element */
    width: 40px;
    height: 58px;
    cursor: pointer;
    background: url(Images/IMG_arrow.png) center center no-repeat;
    overflow: hidden;}
	
.jssora22l { background-position: -10px -31px; }
.jssora22r { background-position: -70px -31px; }
.jssora22l:hover { background-position: -130px -31px; }
.jssora22r:hover { background-position: -190px -31px; }
.jssora22l.jssora22ldn { background-position: -250px -31px; }
.jssora22r.jssora22rdn { background-position: -310px -31px; }

.jssora22l.jssora22lds { background-position: -10px -31px; opacity: .3; pointer-events: none; }
.jssora22r.jssora22rds { background-position: -70px -31px; opacity: .3; pointer-events: none; }


#loginDiv {
	   width: 300px;
	   position: absolute;
	   right: 80px;
	   top: 12px;
	   z-index: 100;
	   border: 1px solid navy;
}


 /* paragraph that shows the text "Login" which is clicked on to display/remove the form */
#login {
	margin: 0;
	cursor: pointer;
	background-color: rgb(255,255,255);
	padding: 5px 0 2px 30px;
}

#login:hover{
	     background-color: rgb(110,138,195);	
}


 /*  plus sign icon for login form */
.plus {
	background: url(Images/img_open.png) no-repeat 8px 7px;
	background-color: rgb(110,138,195);	
}

 /* minus sign icon for login form */
.minus {
	background: url(Images/img_close.png) no-repeat 8px 7px;
}


/*form is hidden when the page loads */
#loginDiv form {
	        padding: 10px 10px 10px 10px;
	        display: none;							/*--none--*/
	        background-color: rgb(255,255,255);
}

#loginDiv label {
	         display: block;
	         width: 100px;
	         margin: 0 15px 0 0;
}

#loginDiv input {
	         font-size: 1.2em;
		 border: 1px solid navy;
}

#loginDiv input:focus {
                       background-color: rgb(110,138,195);
		       border: 2px solid navy;
}

#loginDiv input[type=button] {
	                      width: 100px;
}

</style>   
</head>

<body style="padding:0px; margin:0px; background-color:#fff;font-family:Arial, sans-serif">
<?php
include "MyraTrain_DB.php";
if (isset($_POST['Login_Submit']) && $_POST['Login_Submit'] == 'Submit')
{
$User_ID = strtoupper(trim($_POST['Username']));
$User_PWD = strtoupper(trim($_POST['Password']));	


$query_GetUserName = mysqli_query($con, "SELECT * FROM User_Lists WHERE User_ID=\"$User_ID\"");															
$GetUserName_Row = mysqli_fetch_array($query_GetUserName);
$User_Status = strtoupper(trim($GetUserName_Row['User_Status']));	
$UserDatabase_PWD = strtoupper(trim($GetUserName_Row['User_PWD']));

		  
$User_Num = mysqli_num_rows($query_GetUserName);
	
	if ($User_Num<1) 
	{
	$ERR_MSG = "Invalid User Name";				
	echo "<script>window.location='index.php?ERR_MSG=$ERR_MSG&User_ID=$User_ID'</script>";
	}
	elseif ($User_Status != "ACTIVE")
	{
	$ERR_MSG = "Inactive User";				
	echo "<script>window.location='index.php?ERR_MSG=$ERR_MSG&User_ID=$User_ID'</script>";
	}
	elseif ($UserDatabase_PWD == $User_PWD)
	{
	$ERR_MSG = "Login is successfull";
	$_SESSION["User_ID"] = $User_ID;			
	echo "<script>window.location='Home20171.php?ERR_MSG=$ERR_MSG&User_ID=$User_ID'</script>";
	}
	
}
else
{
?>


<!------------------------------------------------------------------------------------------------------------>
<!------------------ Login box ------control top to low all slides--------------------------------------------> 
<!------------------------------------------------------------------------------------------------------------>  

<div id="loginDiv">
       <!-- when this is clicked on the below form should be displayed and plus sign should change to minus sign-->
       <p id="login" class="plus">Login</p> 
       
 <form name="Feedback_Form" action="" method="post" onkeydown="if(event.keyCode==13 || event.keyCode==116) return false;" onsubmit="Check_RdU()">
	   <p>
	      <label for="username">Username:</label>
	      <input type="text" name="Username" id="username">
	   </p>
	
	   <p>
	      <label for="pw">Password:</label>
	      <input type="password" name="Password" id="pw">
	   </p>
	
	   <p>
          <input type="submit" name="Login_Submit" value="Submit">
	   </p>
	   
	   <!-- placeholder for response if form data is correct/incorrect --> 
	  <p id="error"> </p>
       </form>
</div>


<!------------------------------------------------------------------------------------------------------------>
<!------------------ Slides ------flash from left to right----------------------------------------------------> 
<!------------------------------------------------------------------------------------------------------------> 

<div id="jssor_1" style="position: relative; margin: 0 auto; top: 80px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">

        
			<!-- step 1: Loading Screen ------control top to low all slides------------->        
<div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
	<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
    <div style="position:absolute;display:block;background:url('Images//loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
</div>
        

			<!---2nd container for slides------------->
<div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
                        
			<!--first slide-------------------------------->           
<div data-p="225.00" style="display: none;">		<!---data-t controls delays--->
	<img data-u="image" src="Images/IMG_11.jpg" />
    <div style="position: absolute; top: 30px; left: 20px; width: 520px; height: 120px; font-size: 40px; color: #ffffff; line-height: 60px;">VISUALIZE <br />PRODUCT DEVELOPMENT</div>																<!---title on the top left----->
    <div style="position: absolute; top: 300px; left: 30px; width: 480px; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;">Templates for hardware, software, hardware+software, system, disposable...</div>
</div>
			<!--end of 1st slide-->
            
            
			<!--2nd slide-------------------------------->
<div data-p="225.00" style="display: none;">
<img data-u="image" src="Images/IMG_21.jpg" />
<div style="position: absolute; top: 30px; left: 30px; width: 480px; height: 120px; font-size: 45px; color: #ffffff; line-height: 60px;">INFORMATION <br />UNDER ONE ROOF</div>																<!---title on the top left----->
<div style="position: absolute; top: 300px; left: 30px; width: 480px; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;">Develiverables, human power, schedules, relevants, approverlists...</div>


</div>
            
			<!--3rd slide-------------------------------->
<div data-p="225.00" style="display: none;">
<img data-u="image" src="Images/IMG_31.jpg" />
<div style="position: absolute; top: 30px; left: 30px; width: 480px; height: 120px; font-size: 45px; color: #ffffff; line-height: 60px;">CUSTOMIZE <br />PROJECT ROADMAP</div>																<!---title on the top left----->
<div style="position: absolute; top: 300px; left: 30px; width: 480px; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;">It is so simply, you just need to drag and drop deliverables...</div>
</div>
            
            
			<!--4th slide-------------------------------->            
<div data-p="225.00" style="display: none;">
<img data-u="image" src="Images/IMG_41.jpg" />
<div style="position: absolute; top: 30px; left: 30px; width: 500px; height: 120px; font-size: 45px; color: #ffffff; line-height: 60px;">MONITOR <br />PROJECT OPERATION</div>																<!---title on the top left----->
<div style="position: absolute; top: 300px; left: 30px; width: 480px; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;">Real time dashboard, statistical charts, deliverable status, critical path... </div>
</div>

</div>   <!--end of slides----->
        
        
<!-- Bullet Navigator -->
<div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
       
   
   
</div>   <!---end of max container--->
<?php
}
mysql_free_result($AUP_CheckSQL); 
mysql_close($con);

?>
    <!-- #endregion Jssor Slider End -->
</body>
</html>
