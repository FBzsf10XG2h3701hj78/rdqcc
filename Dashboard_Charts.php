<?php
/*
session_start(); 
if (!isset($_SESSION["User_ID"]) || $_SESSION["User_ID"]=="")
	{
		$ERR_MSG = "Authorization Failed !";
		echo "<script>window.location='index.php?ERR_MSG=$ERR_MSG'</script>";
	} 
$User_ID = strtoupper($_SESSION["User_ID"]); */
?>

<html>
<head>
<link href="CSS/MyraTrain20171.css" rel="stylesheet" type="text/css" />
<?php /*** Google Charts...will replace eventually with pChart ***/ ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
function Select_a_Project()
{
 var Selected_Project = document.getElementById("Project_Name").value;				
 window.open("Dashboard_Charts.php?Project="+Selected_Project, "_self"); 
}
</script>

<style type="text/css">
/*---Global---*/
*{margin:0;padding:0}
</style>	  

</head>
  
  
<body>
<?php
$Project_Name = 'SampleProject_self';
/**********
include "MyraTrain_DB.php";
$Project_Name = $_GET['Project'];			//Operation=creation-->ID=project_Name-->server=Project---->php=$project_Name

														//prepare data for Figure 1: pie chart
$query_DHF ="SELECT Project_Roadmap.Task_Abb 
  		FROM Project_Roadmap
  		INNER JOIN PDP_Task_Output ON Project_Roadmap.Task_Abb=PDP_Task_Output.Task_Abb
  		WHERE Project_Roadmap.Project_Name=\"$Project_Name\" AND PDP_Task_Output.Output_Doc_Type ='DHF' AND Project_Roadmap.Layer !='Hidden'";
$query_DHFInfo = mysqli_query($con, $query_DHF);
$DHF_Num = mysqli_num_rows($query_DHFInfo);

$query_NRDHF ="SELECT Project_Roadmap.Task_Abb 
  		FROM Project_Roadmap
  		INNER JOIN PDP_Task_Output ON Project_Roadmap.Task_Abb=PDP_Task_Output.Task_Abb
  		WHERE Project_Roadmap.Project_Name=\"$Project_Name\" AND PDP_Task_Output.Output_Doc_Type ='DHF' AND Project_Roadmap.Layer ='Hidden'";
$query_NRDHFInfo = mysqli_query($con, $query_NRDHF);
$NRDHF_Num = mysqli_num_rows($query_NRDHFInfo);  			

$query_PHF ="SELECT Project_Roadmap.Task_Abb 
  		FROM Project_Roadmap
  		INNER JOIN PDP_Task_Output ON Project_Roadmap.Task_Abb=PDP_Task_Output.Task_Abb
  		WHERE Project_Roadmap.Project_Name=\"$Project_Name\" AND PDP_Task_Output.Output_Doc_Type ='PHF' AND Project_Roadmap.Layer !='Hidden'";
$query_PHFInfo = mysqli_query($con, $query_PHF);
$PHF_Num = mysqli_num_rows($query_PHFInfo);

													//prepare data for Figure 2: bar chart

$query_GetSignedDHF = mysqli_query($con, "SELECT Task_Abb FROM Project_Roadmap WHERE Project_Name = \"$Project_Name\" AND Layer ='Signed'");
$SignedDHF_Num = mysqli_num_rows($query_GetSignedDHF);


$query_GetDelayDHF = mysqli_query($con, "SELECT Task_Abb FROM Project_Roadmap WHERE Project_Name = \"$Project_Name\" AND Layer ='Delayed'");
$DelayDHF_Num = mysqli_num_rows($query_GetDelayDHF);

$query_GetOngoingDHF = mysqli_query($con, "SELECT Task_Abb FROM Project_Roadmap WHERE Project_Name = \"$Project_Name\" AND Layer ='Ongoing'");
$OngoingDHF_Num = mysqli_num_rows($query_GetOngoingDHF);

												//prepare data for Figure 3: Time-Frame chart
$query_PhaseInfo = "SELECT * FROM Project_Info WHERE Project_Name=\"$Project_Name\"";		  												
$PhaseInfo = mysqli_query($con, $query_PhaseInfo);	               
$PhaseInfo_Row = mysqli_fetch_array($PhaseInfo);

$ProjectStart_Date = $PhaseInfo_Row['ProjectStart_Date'];
$ExitStage0_Date = $PhaseInfo_Row['ExitStage0_Date'];
$ExitStage1_Date = $PhaseInfo_Row['ExitStage1_Date'];
$ExitStage2_Date = $PhaseInfo_Row['ExitStage2_Date'];
$COReleaseStage3_Date = $PhaseInfo_Row['COReleaseStage3_Date'];
$STED510kStage4_Date = $PhaseInfo_Row['STED510kStage4_Date'];
$LCRStartStage4_Date = $PhaseInfo_Row['LCRStartStage4_Date'];
$GAStartStage5_Date = $PhaseInfo_Row['GAStartStage5_Date'];
**********/

$DHF_Num = '1000';
$NRDHF_Num = '125';
$PHF_Num = '250';
$SignedDHF_Num = '90';
$DelayDHF_Num = '82';
$OngoingDHF_Num = '828';

$ProjectStart_Date = '2017-01-01';
$ExitStage0_Date = '2017-02-01';
$ExitStage1_Date = '2017-03-01';
$ExitStage2_Date = '2017-04-01';
$COReleaseStage3_Date = '2017-02-01';
$STED510kStage4_Date = '2017-02-01';
$LCRStartStage4_Date = '2017-02-01';
$GAStartStage5_Date = '2017-02-01';

/**********
//prepare data for Table 1: Cross functional team (remove the team without memeber)
$query_TeamInfo = "SELECT * FROM Project_Info WHERE Project_Name=\"$Project_Name\"";		  												
$TeamInfo = mysqli_query($con, $query_TeamInfo);	               
$TeamInfo_Row = mysqli_fetch_array($TeamInfo);

$Project_Manager = $TeamInfo_Row['Project_Manager'];
$Product_Manager = $TeamInfo_Row['Product_Manager'];
$Regulatory = $TeamInfo_Row['Regulatory']; 	
$Technical_Communicator = $TeamInfo_Row['Technical_Communicator']; 	
$Legal = $TeamInfo_Row['Legal'];	
$Patent = $TeamInfo_Row['Patent'];	
$System_Engineer = $TeamInfo_Row['System_Engineer']; 	
$Hardware_Engineer = $TeamInfo_Row['Hardware_Engineer'];	
$RD_Eng = $TeamInfo_Row['RD_Eng'];	
$Software_Engineer = $TeamInfo_Row['Software_Engineer'];  	
$Human_Factor_Engineer = $TeamInfo_Row['Human_Factor_Engineer']; 	
$Clinical = $TeamInfo_Row['Clinical'];	
$Manufacturing = $TeamInfo_Row['Manufacturing'];	
$Quality_Engineer = $TeamInfo_Row['Quality_Engineer'];	
$Supplier_Quality_Engineer = $TeamInfo_Row['Supplier_Quality_Engineer'];	
$Risk_Management_Engineer = $TeamInfo_Row['Risk_Management_Engineer']; 	
$Software_Quality_Engineer = $TeamInfo_Row['Software_Quality_Engineer']; 
**********/

$Project_Manager = 'Jinxing Xiao';
$Product_Manager = 'Jinxing Xiao';
$Regulatory = 'Jinxing Xiao'; 	
$Technical_Communicator = 'Jinxing Xiao'; 	
$Legal = 'Jinxing Xiao';	
$Patent = 'Jinxing Xiao';	
$System_Engineer = 'Jinxing Xiao'; 	
$Hardware_Engineer = 'Jinxing Xiao';	
$RD_Eng = 'Jinxing Xiao';	
$Software_Engineer = 'Jinxing Xiao';  	
$Human_Factor_Engineer = 'Jinxing Xiao'; 	
$Clinical = 'Jinxing Xiao';	
$Manufacturing = 'Jinxing Xiao';	
$Quality_Engineer = 'Jinxing Xiao';	
$Supplier_Quality_Engineer = 'Jinxing Xiao';	
$Risk_Management_Engineer = 'Jinxing Xiao'; 	
$Software_Quality_Engineer = 'Jinxing Xiao';

/***********												//Prepare data for table 2(select project) and table 3 (bulletin boards)
$query_GetProjectInfo = mysqli_query($con, "SELECT * FROM Project_Info WHERE Project_Name = \"$Project_Name\"");
$GetProjectInfo_Row = mysqli_fetch_array($query_GetProjectInfo);	

$Project_Category = $GetProjectInfo_Row['Project_Category']; 
$Project_Priority = $GetProjectInfo_Row['Project_Priority'];
$Project_Status = $GetProjectInfo_Row['Project_Status'];
$Action_Response = $GetProjectInfo_Row['Action_Response'];
$KeyActivities = $GetProjectInfo_Row['KeyActivities'];
$Risks = $GetProjectInfo_Row['Risks'];
*************/

$Project_Category = 'Category'; 
$Project_Priority = 'Priority';
$Project_Status = 'Status';
$Action_Response = 'Response';
$KeyActivities = 'Activities';
$Risks = 'Risks';

function Select_ButtonOption($Operation, $Operation_Title, $Operation_Ary_Desc, $Operation_Ary_Value) 
	{
	$Operation_ID = $_REQUEST[$Operation];	//get name of selected Operation type from above javascript for internal (equiv $Operation_Type)
	$Operation_Ary_ValueComing = array($Operation_ID);			//variable within array shall have no quoation (waste 3 hours)		
	$Operation_Ary_L = count($Operation_Ary_Value);
	//---------------------------
	for ($cj=0; $cj<$Operation_Ary_L; $cj++)							//This loop is used to obtain the description of selected $Operation_ID
		{
		if ($Operation_Ary_Value[$cj] == $Operation_ID)
			{
			$Operation_ID_Desc = $Operation_Ary_Desc[$cj];				//$Operation_ID_Desc shall be displayed at button
			}
		}
	$Operation_Ary_ValueDesc = array($Operation_ID_Desc);	
	
	$Operation_Ary_DifValue = array_diff($Operation_Ary_Value, $Operation_Ary_ValueComing);	//the difference is used to display rest of options
	$Operation_Ary_DifDesc = array_diff($Operation_Ary_Desc, $Operation_Ary_ValueDesc);		//in pull down table after one option is selected
	$Operation_Ary_L = count($Operation_Ary_DifValue);		
	//--------------------------						
	if (isset($Operation_ID) && $Operation_ID<>'') 					//if click on an option ($Operation_ID is set(isset) and not(<>) blank
		{																	
		echo "<option value='$Operation_ID'>$Operation_ID_Desc</option>";		//hold the selected option as table default (display)				
		for ($ci=0; $ci<=$Operation_Ary_L; $ci++)
			{
			if (isset($Operation_Ary_DifValue[$ci])) 						//skip the blank row for "title: please select one"
				{		
				echo "<option value='$Operation_Ary_DifValue[$ci]'>$Operation_Ary_DifDesc[$ci]</option>";//display rest options only (no blank) 			
				}
			}
		}
	else	
		{
	?>
    	<option value=""><?php echo $Operation_Title?></option>
    <?php
		$Operation_Ary_M = count($Operation_Ary_Value);	
		for ($cj=0; $cj<$Operation_Ary_M; $cj++)
			{
	?>									
   <option value="<?php echo $Operation_Ary_Value[$cj] ?>" ID="<?php echo $Operation_Ary_Value[$cj] ?>"><?php echo $Operation_Ary_Desc[$cj] ?></option>;
    <?php
			}
		} //end of 'else'
	}	//end of the function	
?>

<!-----------------------------------------start to run program--(if detect a select a project)------------------------>


<div ID="Roadmap_Container">
<form>
<input type="hidden" id="DHF_Num" name="DHF_Num" value="<?php echo $DHF_Num;?>" />				<!--assign values for pie chart--->
<input type="hidden" id="NRDHF_Num" name="NRDHF_Num" value="<?php echo $NRDHF_Num;?>" />
<input type="hidden" id="PHF_Num" name="PHF_Num" value="<?php echo $PHF_Num;?>" />

<input type="hidden" id="SignedDHF_Num" name="SignedDHF_Num" value="<?php echo $SignedDHF_Num;?>" />	<!--assign values for bar chart--->
<input type="hidden" id="DelayDHF_Num" name="DelayDHF_Num" value="<?php echo $DelayDHF_Num;?>" />
<input type="hidden" id="OngoingDHF_Num" name="OngoingDHF_Num" value="<?php echo $OngoingDHF_Num;?>" />

																								<!--assign values for time-frame chart--->
<input type="hidden" id="ProjectStart_Date" name="ProjectStart_Date" value="<?php echo $ProjectStart_Date;?>" /> 
<input type="hidden" id="ExitStage0_Date" name="ExitStage0_Date" value="<?php echo $ExitStage0_Date;?>" />
<input type="hidden" id="ExitStage1_Date" name="ExitStage1_Date" value="<?php echo $ExitStage1_Date;?>" />
<input type="hidden" id="ExitStage2_Date" name="ExitStage2_Date" value="<?php echo $ExitStage2_Date;?>" />
<input type="hidden" id="COReleaseStage3_Date" name="COReleaseStage3_Date" value="<?php echo $COReleaseStage3_Date;?>" />
<input type="hidden" id="STED510kStage4_Date" name="STED510kStage4_Date" value="<?php echo $STED510kStage4_Date;?>" />
<input type="hidden" id="LCRStartStage4_Date" name="LCRStartStage4_Date" value="<?php echo $LCRStartStage4_Date;?>" />
<input type="hidden" id="GAStartStage5_Date" name="GAStartStage5_Date" value="<?php echo $GAStartStage5_Date;?>" />


<input type="hidden" id="Project_Manager" name="Project_Manager" value="<?php echo $Project_Manager;?>" /> <!--assign values for team table 1--->
<input type="hidden" id="Product_Manager" name="Product_Manager" value="<?php echo $Product_Manager;?>" />
<input type="hidden" id="Regulatory" name="Regulatory" value="<?php echo $Regulatory;?>" />
<input type="hidden" id="Technical_Communicator" name="Technical_Communicator" value="<?php echo $Technical_Communicator;?>" />
<input type="hidden" id="Legal" name="Legal" value="<?php echo $Legal;?>" />
<input type="hidden" id="Patent" name="Patent" value="<?php echo $Patent;?>" />
<input type="hidden" id="System_Engineer" name="System_Engineer" value="<?php echo $System_Engineer;?>" />
<input type="hidden" id="Hardware_Engineer" name="Hardware_Engineer" value="<?php echo $Hardware_Engineer;?>" />
<input type="hidden" id="RD_Eng" name="RD_Eng" value="<?php echo $RD_Eng;?>" />
<input type="hidden" id="Software_Engineer" name="Software_Engineer" value="<?php echo $Software_Engineer;?>" />
<input type="hidden" id="Human_Factor_Engineer" name="Human_Factor_Engineer" value="<?php echo $Human_Factor_Engineer;?>" />
<input type="hidden" id="Clinical" name="Clinical" value="<?php echo $Clinical;?>" />
<input type="hidden" id="Manufacturing" name="Manufacturing" value="<?php echo $Manufacturing;?>" />
<input type="hidden" id="Quality_Engineer" name="Quality_Engineer" value="<?php echo $Quality_Engineer;?>" />
<input type="hidden" id="Supplier_Quality_Engineer" name="Supplier_Quality_Engineer" value="<?php echo $Supplier_Quality_Engineer;?>" />
<input type="hidden" id="Risk_Management_Engineer" name="Risk_Management_Engineer" value="<?php echo $Risk_Management_Engineer;?>" />
<input type="hidden" id="Software_Quality_Engineer" name="Software_Quality_Engineer" value="<?php echo $Software_Quality_Engineer;?>" />

<input type="hidden" id="Action_Response" name="Action_Response" value="<?php echo $Action_Response;?>" /> <!--assign values for note table 3-->
<input type="hidden" id="KeyActivities" name="" value="<?php echo $KeyActivities;?>" />
<input type="hidden" id="Risks" name="Risks" value="<?php echo $Risks;?>" />
</form>    
<table>
 
<!---if select project name, show all graphs----------------------------------->
<!----------------------------------------------------------------------------->
<?php

if (isset($Project_Name) && $Project_Name<>'') 
{	
?> 

<script type="text/javascript">
      // Load all corechart and tables.. in a one.
google.charts.load('current', {'packages':['corechart', 'table', 'timeline']});
      // Draw the pie chart for deliverable percent is loaded.
	  
<!-------------------------Draw Figure 1: Draw Deliverable percent (pie chart)--------------------->
<!------------------------------------------------------------------------------------------------->	  
<?php /*****
var DHFNum = document.getElementById("DHF_Num").value;
var NRDHFNum = document.getElementById("NRDHF_Num").value;
var PHFNum = document.getElementById("PHF_Num").value; 
    
google.charts.setOnLoadCallback(drawPie);

    // Callback that draws the pie chart for deliverable pecent.

function drawPie() 
{	
        DHFNum = eval(DHFNum);							//eval function is used to initate the variables within function 
		NRDHFNum = eval(NRDHFNum);
		PHFNum = eval(PHFNum);
		var data = new google.visualization.DataTable();
        data.addColumn('string', 'Deliverable Types');
        data.addColumn('number', 'Reports');
		
		data.addRows([
          ['Required DHF',DHFNum],	//DHFNum
		  ['NR DHF',NRDHFNum],			//NRDHFNum
          ['9000s',PHFNum]			//PHFNum
        ]);

        // Set options
        var options = {title:'DHF vs. Business Deliverables',
                       width:350,
                       height:300};

        // Instantiate and draw the chart for Sarah's pizza.
        var chart = new google.visualization.PieChart(document.getElementById('DeliverableAmount_Pie'));
        chart.draw(data, options);
}
****/ ?>

<!----------------------draw Figure 2: Draw Deliverable status (column chart)---------------------->
<!------------------------------------------------------------------------------------------------->  
<?php/****
       // Draw the pie chart for deliverable percent is loaded.
		var SignedDHF_Num = document.getElementById("SignedDHF_Num").value;
		var DelayDHF_Num = document.getElementById("DelayDHF_Num").value; 
	    var OngoingDHF_Num = document.getElementById("OngoingDHF_Num").value;  
google.charts.setOnLoadCallback(drawStatusBar);
	  
function drawStatusBar() {
	
		SignedDHF_Num = eval(SignedDHF_Num);
		DelayDHF_Num  = eval(DelayDHF_Num);
		OngoingDHF_Num = eval(OngoingDHF_Num);
	
      var data = google.visualization.arrayToDataTable([
        ["Status", "Amount", { role: "style" } ],
        ["Signed", SignedDHF_Num, "green"],
        ["Delayed", DelayDHF_Num, "red"],
        ["Ongoing", OngoingDHF_Num, "blue"]		<!--color: #e5e4e2-->
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Project DHF deliverable status",
        width: 450,
        height: 300,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("DeliverableStatus_Bar"));
      chart.draw(view, options);
  }
****/?>
 
<!-------------------------Draw Figure 3: Project Time Frame chart (pie chart)--------------------->
<!------------------------------------------------------------------------------------------------->
      var ProjectStart_Date = new Date(document.getElementById("ProjectStart_Date").value);
	  var ProjectStart_Date_Year = ProjectStart_Date.getFullYear();
	  var ProjectStart_Date_Month = ProjectStart_Date.getMonth();
 	  var ProjectStart_Date_Day = ProjectStart_Date.getDate();
  
	  var ExitStage0_Date = new Date(document.getElementById("ExitStage0_Date").value);
	  var ExitStage0_Date_Year = ExitStage0_Date.getFullYear();
	  var ExitStage0_Date_Month = ExitStage0_Date.getMonth();
 	  var ExitStage0_Date_Day = ExitStage0_Date.getDate();
	  
	  
	  var ExitStage1_Date = document.getElementById("ExitStage1_Date").value;
	  var ExitStage2_Date = document.getElementById("ExitStage2_Date").value;
	  var COReleaseStage3_Date = document.getElementById("COReleaseStage3_Date").value;
	  var STED510kStage4_Date = document.getElementById("STED510kStage4_Date").value;
	  var LCRStartStage4_Date = document.getElementById("LCRStartStage4_Date").value;
	  
	  var GAStartStage5_Date = new Date(document.getElementById("GAStartStage5_Date").value);
	  var GAStartStage5_Date_Year = GAStartStage5_Date.getFullYear();
	  var GAStartStage5_Date_Month = GAStartStage5_Date.getMonth();
 	  var GAStartStage5_Date_Day = GAStartStage5_Date.getDate();
	  
	  
	    
	  google.charts.setOnLoadCallback(timeFrame);

      function timeFrame() {
		  ProjectStart_Date_Year = eval(ProjectStart_Date_Year);
		  ProjectStart_Date_Month = eval(ProjectStart_Date_Month);
		  ProjectStart_Date_Day = eval(ProjectStart_Date_Day);
		  

	  	  ExitStage0_Date_Year = eval(ExitStage0_Date_Year);
	      ExitStage0_Date_Month = eval(ExitStage0_Date_Month);
 	      ExitStage0_Date_Day = eval(ExitStage0_Date_Day);
		  
		  
		  GAStartStage5_Date_Year = eval(GAStartStage5_Date_Year);
		  GAStartStage5_Date_Month = eval(GAStartStage5_Date_Month);
		  GAStartStage5_Date_Day = eval(GAStartStage5_Date_Day);
		
		  var container = document.getElementById("Project_TimeFrame");
       	  var chart = new google.visualization.Timeline(container);
          var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({ type: 'string', id: 'Project' });
        dataTable.addColumn({ type: 'date', id: 'Start' });
        dataTable.addColumn({ type: 'date', id: 'End' });
        dataTable.addRows([
        [ 'Megneto', new Date(ProjectStart_Date_Year,ProjectStart_Date_Month,ProjectStart_Date_Day), new Date(ExitStage0_Date_Year,ExitStage0_Date_Month,ExitStage0_Date_Day)],
		[ 'Megneto', new Date(ExitStage0_Date_Year,ExitStage0_Date_Month,ExitStage0_Date_Day+2), new Date(GAStartStage5_Date_Year,GAStartStage5_Date_Month,GAStartStage5_Date_Day)]
		  ]);

        chart.draw(dataTable);
       

      } 

<!-------------------------Table 1: Cross functional team table------------------------------------>
<!------------------------------------------------------------------------------------------------->
		var Project_Manager = document.getElementById("Project_Manager").value;	 //for string variables, not need to initiate within funciton
		var Product_Manager = document.getElementById("Product_Manager").value;
		var Regulatory = document.getElementById("Regulatory").value;
		var Technical_Communicator = document.getElementById("Technical_Communicator").value;
		var Legal = document.getElementById("Legal").value;
		var Patent = document.getElementById("Patent").value;
		var System_Engineer = document.getElementById("System_Engineer").value;
		var Hardware_Engineer = document.getElementById("Hardware_Engineer").value;
		var RD_Eng = document.getElementById("RD_Eng").value;
		var Software_Engineer = document.getElementById("Software_Engineer").value;
		var Human_Factor_Engineer = document.getElementById("Human_Factor_Engineer").value;
		var Clinical = document.getElementById("Clinical").value;
		var Manufacturing = document.getElementById("Manufacturing").value;
		var Quality_Engineer = document.getElementById("Quality_Engineer").value;
		var Supplier_Quality_Engineer = document.getElementById("Supplier_Quality_Engineer").value;
		var Risk_Management_Engineer = document.getElementById("Risk_Management_Engineer").value;
		var Software_Quality_Engineer = document.getElementById("Software_Quality_Engineer").value;
		
      google.charts.setOnLoadCallback(drawTeam);

      function drawTeam() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Cross Functional Team');
        data.addColumn('string', '');
        data.addRows([
          ['Project Manager',Project_Manager],
		  ['Product Manager',Product_Manager],
		  ['Regulatory',Regulatory],
	 	  ['System Engineer',System_Engineer],
	      ['Hardware Engineer',Hardware_Engineer],
		  ['Software Engineer',Software_Engineer],
		  ['UXD',Human_Factor_Engineer],
	      ['Clinical',Clinical],
		  ['Risk Management',Risk_Management_Engineer],
		  ['Quality Engineer', Quality_Engineer]
        ]);

        var table = new google.visualization.Table(document.getElementById('CrossFuncton_Table'));

        table.draw(data, {width:'350px', height:'300px'});
      }
	  		
	  
	  
<!------------------------draw Table 3: Bulletin Boards ------------------------------------------------------------------->
<!--The reason to place the javascript here is to ensure after taking data from mysql then to drawing the chart------------>
<!------------------------------------------------------------------------------------------------------------------------->
		 var Action_Response  = document.getElementById("Action_Response").value;
		  var KeyActivities = document.getElementById("KeyActivities").value; 
		  var Risks = document.getElementById("Risks").value;
		  
      google.charts.setOnLoadCallback(drawBulletinBoard);

      function drawBulletinBoard() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', '');
        data.addColumn('string', 'Bulletin Boards');
        data.addRows([
          ['Action & Response',Action_Response],
		  ['Key Activities',KeyActivities],
		  ['Risks',Risks],
	 	 /* ['Delays','Regulatory Compliance Plan, Regulatory Compliance Matrix'],*/
        ]);

        var table = new google.visualization.Table(document.getElementById('BulletinBoard_Table'));

        table.draw(data, {width:'450px', height:'300px'});
      }

</script>

<?php
// @see http://flatuicolors.com/
$override_colors = [
    ["R"=>41, "G"=>128,"B"=>185,"Alpha"=>100], // rgb(41, 128, 185)
    ["R"=>39, "G"=>174, "B"=>96, "Alpha"=>100], // rgb(39, 174, 96)
    ["R"=>241, "G"=>196, "B"=>15, "Alpha"=>100], // rgb(241, 196, 15)
    ["R"=>230, "G"=>126, "B"=>34, "Alpha"=>100], // rgb(230, 126, 34)
    ["R"=>192, "G"=>57, "B"=>43, "Alpha"=>100], // rgb(192, 57, 43)
    ["R"=>142, "G"=>68, "B"=>173, "Alpha"=>100], // rgb(142, 68, 173)
    ["R"=>44, "G"=>62, "B"=>80, "Alpha"=>100], // rgb(44, 62, 80)
    ["R"=>149, "G"=>165, "B"=>166, "Alpha"=>100], // rgb(149, 165, 166)
    ["R"=>189, "G"=>195, "B"=>199, "Alpha"=>100] // rgb(189, 195, 199)
];

/**
 * 1. Create Pie chart
 */
/* pChart library inclusions */ 
 include("pChart2.1.4/class/pData.class.php"); 
 include("pChart2.1.4/class/pDraw.class.php"); 
 include("pChart2.1.4/class/pPie.class.php"); 
 include("pChart2.1.4/class/pImage.class.php"); 
 
 $pieChartData = [
     'chart_title' => 'DHF vs. Business Deliverables',
     'chart_title_height_from_top' => 15,
     'box_width' => 450,
     'box_height' => 300,
     'values' => [$DHF_Num, $NRDHF_Num, $PHF_Num],
     'legend_labels' => ['Required DHF', 'NR DHF', '9000s']
 ];
 $pieChartData['labels'] = [
     $pieChartData['values'][0] . ' (' . number_format($pieChartData['values'][0]/array_sum($pieChartData['values'])*100, 1) . '%)',
     $pieChartData['values'][1] . ' (' . number_format($pieChartData['values'][1]/array_sum($pieChartData['values'])*100, 1) . '%)',
     $pieChartData['values'][2] . ' (' . number_format($pieChartData['values'][2]/array_sum($pieChartData['values'])*100, 1) . '%)',
 ];
 
 /* Create and populate the pData object */ 
 $MyData = new pData();    
 $MyData->addPoints($pieChartData['values'],"Values");   
// $MyData->setSerieDescription("ScoreA","Application A"); 

 /* Define the absissa serie */ 
 $MyData->addPoints($pieChartData['labels'],"Labels"); 
 $MyData->setAbscissa("Labels"); 

 /* Create the pChart object */ 
 $myPicture = new pImage($pieChartData['box_width'], $pieChartData['box_height'],$MyData); 

 /* Draw a solid background */ 
 $Settings = ["R"=>255, "G"=>255, "B"=>255, "Dash"=>1, "DashR"=>193, "DashG"=>172, "DashB"=>237]; 
 //$myPicture->drawFilledRectangle(0,0,700,230,$Settings); 

 /* Draw a gradient overlay */ 
 //$Settings = array("StartR"=>209, "StartG"=>150, "StartB"=>231, "EndR"=>111, "EndG"=>3, "EndB"=>138, "Alpha"=>50); 
 //$myPicture->drawGradientArea(0,0,700,230,DIRECTION_VERTICAL,$Settings); 
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>100)); 

 /* Add a border to the picture */ 
 //$myPicture->drawRectangle(0,0,699,229,array("R"=>0,"G"=>0,"B"=>0)); 

 /* Write the picture title */  
 //$myPicture->setFontProperties(array("FontName"=>"pChart2.1.4/fonts/Silkscreen.ttf","FontSize"=>6)); 
 //$myPicture->drawText(10,13,"pPie - Draw 2D pie charts",array("R"=>255,"G"=>255,"B"=>255)); 

 /* Set the default font properties */  
 $myPicture->setFontProperties(["FontName"=>"pChart2.1.4/fonts/verdana.ttf","FontSize"=>8,"R"=>0,"G"=>0,"B"=>0]); 

 /* Enable shadow computing */  
 //$myPicture->setShadow(TRUE,array("X"=>2,"Y"=>2,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>50)); 

 /* Create the pPie object */  
 $PieChart = new pPie($myPicture,$MyData); 

 /* Draw a simple pie chart */  
 //$PieChart->draw2DPie(120,125,array("SecondPass"=>FALSE)); 

 $PieChart->setSliceColor(0, $override_colors[0]);
 $PieChart->setSliceColor(1, $override_colors[1]);
 $PieChart->setSliceColor(2, $override_colors[2]);
 
 /* Draw an AA pie chart */  
 $PieChart->draw2DPie(
    $pieChartData['box_width']/2,
    $pieChartData['box_height']/2,
    ["DrawLabels"=>TRUE,
     "LabelStacked"=>TRUE,
     "Border"=>TRUE]); 
 
 /* Draw a splitted pie chart */  
 //$PieChart->draw2DPie(560,125,array("WriteValues"=>PIE_VALUE_PERCENTAGE,"DataGapAngle"=>10,"DataGapRadius"=>6,"Border"=>TRUE,"BorderR"=>255,"BorderG"=>255,"BorderB"=>255)); 

 /* Write the legend */ 
 //$myPicture->setFontProperties(array("FontName"=>"pChart2.1.4/fonts/pf_arma_five.ttf","FontSize"=>6)); 
 //$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>20)); 
 //$myPicture->drawText(120,200,"Single AA pass",array("DrawBox"=>TRUE,"BoxRounded"=>TRUE,"R"=>0,"G"=>0,"B"=>0,"Align"=>TEXT_ALIGN_TOPMIDDLE)); 
 $myPicture->drawText($pieChartData['box_width']/2,$pieChartData['chart_title_height_from_top'],$pieChartData['chart_title'], ["DrawBox"=>FALSE,"R"=>0,"G"=>0,"B"=>0,"Align"=>TEXT_ALIGN_TOPMIDDLE]); 

 /* Write the legend box */  
 //$myPicture->setFontProperties(array("FontName"=>"pChart2.1.4/fonts/Silkscreen.ttf","FontSize"=>6,"R"=>255,"G"=>255,"B"=>255)); 
 //$PieChart->drawPieLegend(380,8,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL));
 
 // Output as an image, call it later in HTML with: <img src="DeliverableAmount_Pie.png">
 $myPicture->render("DeliverableAmount_Pie.png");
 
 
/**
 * 2. Create Bar chart
 */
 
// Title: Project DHF deliverable status
// Bottom bar title: Signed, Delayed, Ongoing 

$barChartData = [
     'chart_title' => 'Project DHF deliverable status',
     'chart_title_height_from_top' => 30,
     'box_width' => 450,
     'box_height' => 300,
     'values' => [$SignedDHF_Num, $DelayDHF_Num, $OngoingDHF_Num]
 ];
 $barChartData['labels'] = [
     'Signed',
     'Delayed',
     'Ongoing',
 ];
 
/* Create and populate the pData object */ 
 $MyData = new pData();   
 $MyData->addPoints([$barChartData['values'][0], $barChartData['values'][1], $barChartData['values'][2]], "Probe 1"); 
// $MyData->addPoints([3], "Probe 2"); 
// $MyData->addPoints([22,4,33],"Probe 3"); 
// $MyData->setSerieTicks("Probe 2",4); 
// $MyData->setAxisName(0,"Temperatures"); 
 $MyData->addPoints($barChartData['labels'],"Labels"); 
 $MyData->setSerieDescription("Labels","Months"); 
 $MyData->setAbscissa("Labels"); 

 /* Create the pChart object */ 
 $myPicture = new pImage($barChartData['box_width'],$barChartData['box_height'],$MyData); 

 /* Draw the background */ 
 $Settings = ["R"=>255, "G"=>255, "B"=>255, "Dash"=>1, "DashR"=>190, "DashG"=>203, "DashB"=>107]; 
// $myPicture->drawFilledRectangle(0,0,700,230,$Settings); 

 /* Overlay with a gradient */ 
// $Settings = array("StartR"=>219, "StartG"=>231, "StartB"=>139, "EndR"=>1, "EndG"=>138, "EndB"=>68, "Alpha"=>50); 
// $myPicture->drawGradientArea(0,0,700,230,DIRECTION_VERTICAL,$Settings); 
// $myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80)); 

 /* Add a border to the picture */ 
// $myPicture->drawRectangle(0,0,699,229,array("R"=>0,"G"=>0,"B"=>0)); 
  
 /* Write the picture title */  
// $myPicture->setFontProperties(["FontName"=>"pChart2.1.4/fonts/verdana.ttf","FontSize"=>6]); 
// $myPicture->drawText(10,13,$barChartData['chart_title'], ["R"=>255,"G"=>255,"B"=>255]); 

 /* Write the chart title */  
 $myPicture->setFontProperties(["FontName"=>"pChart2.1.4/fonts/verdana.ttf","FontSize"=>11]); 
 $myPicture->drawText($barChartData['box_width']/2, $barChartData['chart_title_height_from_top'], $barChartData['chart_title'], ["FontSize"=>8, "Align"=>TEXT_ALIGN_BOTTOMMIDDLE]); 

 /* Draw the scale and the 1st chart */ 
 $myPicture->setGraphArea(60, 60, 450, 190); 
 $myPicture->drawFilledRectangle(60, 60, 450, 190, ["R" => 255, "G" => 255, "B" => 255, "Surrounding" => -200, "Alpha" => 10]); 
 $myPicture->drawScale(["DrawSubTicks"=>TRUE]); 
// $myPicture->setShadow(TRUE,["X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10]); 
 $myPicture->setFontProperties(["FontName"=>"pChart2.1.4/fonts/verdana.ttf","FontSize"=>8]); 
 
 $myPicture->drawBarChart([
     "DisplayValues" => TRUE,
//     "DisplayColor" => DISPLAY_AUTO,
     "Rounded" => FALSE,
     "Surrounding" => 30,
     "OverrideColors" => $override_colors
 ]); 
 $myPicture->setShadow(FALSE); 

 /* Draw the scale and the 2nd chart */ 
// $myPicture->setGraphArea(500,60,670,190); 
// $myPicture->drawFilledRectangle(500,60,670,190, ["R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10]); 
// $myPicture->drawScale(["Pos"=>SCALE_POS_TOPBOTTOM,"DrawSubTicks"=>TRUE]); 
// $myPicture->setShadow(TRUE, ["X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10]); 
// $myPicture->drawBarChart(); 
// $myPicture->setShadow(FALSE); 

 /* Write the chart legend */ 
 $myPicture->drawLegend(510,205, ["Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL]); 

 /* Render the picture (choose the best way) */ 
 $myPicture->render("DeliverableStatus_Bar.png");
?>
      
 <tr>									<!---three charts on the first row--->
        <td>
            <div id="DeliverableAmount_Pie" style="border: 1px solid #ccc; margin-top:10px">
                <img src="DeliverableAmount_Pie.png">                                
            </div>
        </td>
        <td>
            <div id="DeliverableStatus_Bar" style="border: 1px solid #ccc; margin-left:10px; margin-top:10px">
                <img src="DeliverableStatus_Bar.png">
            </div>
        </td>
        <td><div id="Project_TimeFrame"     style="border: 1px solid #ccc; width:450px; height: 300px; margin-left:10px; margin-top:10px; vertical-align:central"></div></td>
</tr>
    
<tr>
 <td><div id="CrossFuncton_Table" style="border: 1px solid #ccc; margin-top:10px"></div></td>	<!---table 1: cross functional team-->
    

<!-------------------------Table 2: Build a Project navigation table (show project name, priorty, status, category)-------------->

<td>			<!--nested 2nd table--->
<table ID="Dashboard_ProjectSearch" style="border:1px solid #ccc; margin-left:10px; margin-top:10px; width:450px; height: 300px; background-color:#CCFFCC;"> 
            	<!--<tr style="line-height:3px; background-color:#CCFFCC;"-->
                <tr style="line-height:3px; background-color:#CCFFCC;">
                <td width="40%">&nbsp;</td>		
                <td width="20%">&nbsp;</td>		
                <td width="20%">&nbsp;</td>		
                <td width="20%">&nbsp;</td>	</tr>
				
	<tr>
			<!-------------------2.1 select a project name------------------>     
            
	<td class="Select_ButtonTitle">Select a Project</td> <!--text-indent:0.7em----->
	<td colspan="2">
<?php  	
													//each project_name takes only one row in table Project_Info
$query_GetProjectName = mysqli_query($con, "SELECT Project_Name FROM Project_Info");															

$Project_Num = mysqli_num_rows($query_GetProjectName);
for ($i=0; $i<$Project_Num; $i++)
	{
	$Project_Row = mysqli_fetch_array($query_GetProjectName);
	$Project_Lists[$i] = $Project_Row['Project_Name'];			//create a new array to store all project names
	}
?>

		<div>
						<!--function starting point--->
	<Select ID="Project_Name" name="Project_Name" style="width:92%;" OnChange="Select_a_Project()">
<?php	 
$Operation = 'Project';
$Operation_Title = 'Select a Project';
$Operation_Ary_Desc = $Project_Lists;				//use for display
$Operation_Ary_Value = $Project_Lists;				//use for variables	
Select_ButtonOption($Operation, $Operation_Title, $Operation_Ary_Desc, $Operation_Ary_Value);	//use the function
?>   										
	</select>	 			<!--function ending point---->
		</div>				
		</td>				
		<td>&nbsp;</td>			<!--this is for the last column of nested table-->
	</tr>
    
          
    <tr ID="UserList_Table2_Title" style="background-color:#EAF2FF; line-height:10px; margin-left:5px; margin-right:5px"> <!--background: url(Images/tr.gif) repeat-x;--->
		<td class="UserList_Table2_TitleS">Project Name</td>
		<td class="UserList_Table2_TitleS">Category</td>
		<td class="UserList_Table2_TitleS">Priority</td> 
		<td class="UserList_Table2_TitleS">Status</td>
    </tr>
    
    <tr>
  <?php	      					
	echo "<td style='border-bottom:dotted #093 1px; border-right:dotted #093 1px;'>$Project_Name</td>
		  <td style='border-bottom:dotted #093 1px; border-right:dotted #093 1px;'>$Project_Category</td>
		  <td style='border-bottom:dotted #093 1px; border-right:dotted #093 1px;'>$Project_Priority</td>							
		  <td style='border-bottom:dotted #093 1px; border-right:dotted #093 1px;'>$Project_Status</td>";								
	echo "</tr>";	   					  

 ?>
				<!--------Table 2.2 builds a exit button-------------------------------->
<tr><td colspan="4" style='text-align:right'>
<form action="Home20171.php" method="post">	<!--create a button to exit Roadmap and build a Php bridge to home page-->

<input type="submit" value="Exit" id="Button_ExitChart" />
</form>
</td></tr>

</table>			<!--close nested table-->  
</td>
																		<!-------------end of Table 2------------>

					
																		<!-----------------table 3--------------->
        
<td><div id="BulletinBoard_Table" style="border: 1px solid #ccc; width:450px; height: 300px; margin-left:10px; margin-top:10px;"></div></td>    
</tr>									<!---closed on 2nd row --->
    
<?php
 }  			/*-- end of if---*/ 
 
 
 //----------------if no select project name, only display table 2 and frames for others------------------------------------------>
 //-------------------------------------------------------------------------------------------------------------------------------
 else
 {
 ?> 
 
 <tr>									<!---three places for pie, bar, and time frame charts--->
        <td><div style="border: 1px solid #ccc; width:350px; height: 300px; margin-top:10px"></div></td>
        <td><div style="border: 1px solid #ccc; width:450px; height: 300px; margin-left:10px; margin-top:10px"></div></td>
        <td><div style="border: 1px solid #ccc; width:450px; height: 300px; margin-left:10px; margin-top:10px"></div></td>
</tr>
    
<tr>
 <td><div style="border: 1px solid #ccc; width:350px; height: 300px; margin-top:10px"></div></td>    <!--place for cross functional team-->
    

<!------------------------------------------------------------------------------------------------------------------------------->
<!-------------------------Table 2: Build a Project navigation table (show project name, priorty, status, category)-------------->
<!------------------------------------------------------------------------------------------------------------------------------->
<td>			<!--nested 2nd table--->
<table ID="Dashboard_ProjectSearch" style="border:1px solid #ccc; margin-left:10px; margin-top:10px; width:450px; height: 300px; background-color:#CCFFCC;"> 
            	<!--<tr style="line-height:3px; background-color:#CCFFCC;"-->
                <tr style="line-height:3px; background-color:#CCFFCC;">
                <td width="35%">&nbsp;</td>		
                <td width="15%">&nbsp;</td>		
                <td width="15%">&nbsp;</td>		
                <td width="15%">&nbsp;</td>	</tr>
				
	<tr>    
            
	<td class="Select_ButtonTitle">Select a Project</td> <!--text-indent:0.7em---select a project name-->
	<td colspan="2">
<?php  	
													//each project_name takes only one row in table Project_Info
$query_GetProjectName = mysqli_query($con, "SELECT Project_Name FROM Project_Info");															

$Project_Num = mysqli_num_rows($query_GetProjectName);
for ($i=0; $i<$Project_Num; $i++)
	{
	$Project_Row = mysqli_fetch_array($query_GetProjectName);
	$Project_Lists[$i] = $Project_Row['Project_Name'];			//create a new array to store all project names
	}
?>

		<div>
						<!--function starting point--->
	<Select ID="Project_Name" name="Project_Name" style="width:92%;" OnChange="Select_a_Project()">
<?php	 
$Operation = 'Project';
$Operation_Title = 'Select a Project';
$Operation_Ary_Desc = $Project_Lists;				//use for display
$Operation_Ary_Value = $Project_Lists;				//use for variables	
Select_ButtonOption($Operation, $Operation_Title, $Operation_Ary_Desc, $Operation_Ary_Value);	//use the function
?>   										
	</select>	 			<!--function ending point---->
		</div>				
		</td>				
		<td>&nbsp;</td>			
	</tr>
 </table>   				<!--end of nested table--->
 
 <td><div style="border: 1px solid #ccc; width:450px; height: 300px; margin-left:10px; margin-top:10px"></div></td>    
</tr>									

<?php
 }						//close else
 ?>
 
</table>
</div>
</body>
</html>
